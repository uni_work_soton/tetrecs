package uk.ac.soton.comp1206.component;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Multimedia {
    private static final Logger logger = LogManager.getLogger(Multimedia.class);

    protected MediaPlayer musicPlayer;

    public MediaPlayer getMusicPlayer() {
        return musicPlayer;
    }

    public MediaPlayer getAudioPlayer() {
        return audioPlayer;
    }

    protected MediaPlayer audioPlayer;

    public void playAudio(String path){
        String toPlay = Multimedia.class.getResource("/" + path).toExternalForm();
        logger.info("Attempting to play audio:" + path);

        try{
            Media play = new Media(toPlay);
            audioPlayer = new MediaPlayer(play);
            audioPlayer.play();
        }catch(Exception e){
            logger.error("Couldn't plan audio: " + path);
            e.printStackTrace();
        }
    }

    public void playMusic(String path){
        String toPlay = Multimedia.class.getResource("/music/" + path).toExternalForm();
        logger.info("Attempting to play audio:" + path);

        try{
            Media play = new Media(toPlay);
            musicPlayer = new MediaPlayer(play);
            //Makes music play indefinitely. a start and stop time need to be set for this to work
            musicPlayer.setCycleCount(MediaPlayer.INDEFINITE);
            musicPlayer.setStartTime(Duration.ZERO);
            musicPlayer.setStopTime(play.getDuration());
            musicPlayer.play();
        }catch(Exception e){
            logger.error("Couldn't plan audio: " + path);
            e.printStackTrace();
        }
    }

    public void stopMusic(){
        musicPlayer.stop();
    }
}
