package uk.ac.soton.comp1206.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.soton.comp1206.game.GamePiece;
import uk.ac.soton.comp1206.game.Grid;

public class PieceBoard extends GameBoard{

    private static final Logger logger = LogManager.getLogger(PieceBoard.class);

    /**
     * Create a new PieceBoard, based off a given grid, with a visual width and height.
     *
     * @param grid   linked grid
     * @param width  the visual width
     * @param height the visual height
     */
    public PieceBoard(Grid grid, double width, double height) {
        super(grid, width, height);
    }

    /**
     * Create a new PieceBoard with it's own internal grid, specifying the number of columns and rows, along with the
     * visual width and height.
     *
     * @param cols   number of columns for internal grid
     * @param rows   number of rows for internal grid
     * @param width  the visual width
     * @param height the visual height
     */
    public PieceBoard(int cols, int rows, double width, double height) {
        super(cols, rows, width, height);
    }

    public void clearBoard(){
        for(int row = 0;row<getRows();row++){
            for(int col = 0;col<getCols();col++){
                this.grid.set(col,row,0);
            }
        }
    }

    public void setPiece(GamePiece piece){
        //Gets the "middle" place in the grid to place the piece, in case PieceBoard is ever called
        //with even parameters (custom pieces later on?)
        logger.info("Attempting to play piece onto PieceBoard: " + piece.toString());
        this.grid.playPiece(piece, (int) Math.abs((double)this.getCols()/2), (int) Math.abs((double)this.getRows()/2) );

    }
}
