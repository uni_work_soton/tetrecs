package uk.ac.soton.comp1206.scene;

import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.soton.comp1206.component.PieceBoard;
import uk.ac.soton.comp1206.game.GamePiece;
import uk.ac.soton.comp1206.ui.GamePane;
import uk.ac.soton.comp1206.ui.GameWindow;

public class InstructionsScene extends BaseScene {
    private static final Logger logger = LogManager.getLogger(InstructionsScene.class);

    protected Button backButton = new Button("Back");
    protected ImageView instructions = new ImageView(InstructionsScene.class.getResource("/images/Instructions.png").toExternalForm());

    /**
     * Create a new scene, passing in the GameWindow the scene will be displayed in
     *
     * @param gameWindow the game window
     */
    public InstructionsScene(GameWindow gameWindow) {
        super(gameWindow);
        logger.info("initialising instructions scene");
        gameWindow.addEscapeListener(this);
    }

    /**
     * Initialise this scene. Called after creation
     */
    @Override
    public void initialise() {
        logger.info("Initialising Instructions");
    }

    /**
     * Build the instructions window
     */
    @Override
    public void build() {
        logger.info("Building " + this.getClass().getName());

        root = new GamePane(gameWindow.getWidth(),gameWindow.getHeight());

        var instructionsPane = new VBox();
        instructionsPane.setMaxWidth(gameWindow.getWidth());
        instructionsPane.setMaxHeight(gameWindow.getHeight());
        instructionsPane.getStyleClass().add("instructions-background");
        root.getChildren().add(instructionsPane);

        instructions.setFitWidth(gameWindow.getWidth());
        instructions.setFitHeight(gameWindow.getHeight()*7/8);
        instructionsPane.getChildren().add(instructions);

        var piecesBox = new HBox();
        piecesBox.setMaxHeight(gameWindow.getHeight()/8);
        piecesBox.setMaxWidth(gameWindow.getWidth());
        piecesBox.getStyleClass().add("instructions-background");

        //Looping through first 15 gamePieces
        for(int i = 0;i < 15; i++){
            GamePiece gamePiece = GamePiece.createPiece(i);
            PieceBoard pieceBoard = new PieceBoard(3,3,gameWindow.getWidth()/16,gameWindow.getHeight()/8);
            pieceBoard.setPiece(gamePiece);
            piecesBox.getChildren().add(pieceBoard);
        }
        /**
         * 256 is got from the fact theres 16 gaps (15 pieces), so to have
         * each piece fit perfectly in the window, each gap = (1-(15/16))/16
         * I couldve done something with spacers, but this solution is way easier :)
         *
         * (no it doesnt look perfect because of the line separating each block in
         * each pieceBoard, but it shows all the pieces ok?)
         */
        piecesBox.setSpacing(gameWindow.getWidth()/256);

        instructionsPane.getChildren().add(piecesBox);

    }

    public void escapePressed() {
        gameWindow.loadScene(gameWindow.getMenuScene());
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()== KeyCode.ESCAPE) {
            escapePressed();
        }
    }
}
