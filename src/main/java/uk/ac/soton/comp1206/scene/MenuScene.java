package uk.ac.soton.comp1206.scene;

import javafx.animation.Animation;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.soton.comp1206.ui.GamePane;
import uk.ac.soton.comp1206.ui.GameWindow;

/**
 * The main menu of the game. Provides a gateway to the rest of the game.
 */
public class MenuScene extends BaseScene{

    private static final Logger logger = LogManager.getLogger(MenuScene.class);

    /**
     * Create a new menu scene
     * @param gameWindow the Game Window this will be displayed in
     */
    public MenuScene(GameWindow gameWindow) {
        super(gameWindow);
        logger.info("Creating Menu Scene");

        gameWindow.addEscapeListener(this);
    }

    /**
     * Build the menu layout
     */
    @Override
    public void build() {
        logger.info("Building " + this.getClass().getName());

        root = new GamePane(gameWindow.getWidth(),gameWindow.getHeight());

        var menuPane = new StackPane();
        menuPane.setMaxWidth(gameWindow.getWidth());
        menuPane.setMaxHeight(gameWindow.getHeight());
        menuPane.getStyleClass().add("menu-background");
        root.getChildren().add(menuPane);

        var mainPane = new BorderPane();
        menuPane.getChildren().add(mainPane);

        //Awful title
        var title = new ImageView(MenuScene.class.getResource("/images/TetrECS.png").toExternalForm());
        title.setFitWidth(gameWindow.getWidth()/1.5);
        title.setFitHeight(gameWindow.getHeight()/4);
        mainPane.setTop(title);
        BorderPane.setAlignment(title, Pos.TOP_CENTER);
        BorderPane.setMargin(title, new Insets(gameWindow.getHeight()/15));

        var titleRotate1 = new RotateTransition(Duration.millis(3000),title);
        titleRotate1.setFromAngle(-10);
        titleRotate1.setToAngle(10);

        var titleRotate2 = new RotateTransition(Duration.millis(3000),title);
        titleRotate2.setFromAngle(10);
        titleRotate2.setToAngle(-10);

        var sequenceTitle = new SequentialTransition(titleRotate1,titleRotate2);
        sequenceTitle.setCycleCount(Animation.INDEFINITE);
        sequenceTitle.play();

        var buttons = new VBox();
        mainPane.setCenter(buttons);
        mainPane.setAlignment(buttons,Pos.BOTTOM_CENTER);

        buttons.setAlignment(Pos.CENTER);

        var singlePlayerButton = new Button("Play");
        singlePlayerButton.setOnAction(this::startGame);
        var instructionsButton = new Button("How to play");
        instructionsButton.setOnAction(this::showInstructions);

        buttons.getChildren().add(singlePlayerButton);
        buttons.getChildren().add(instructionsButton);
    }

    /**
     * Initialise the menu
     */
    @Override
    public void initialise() {

    }

    /**
     * Handle when the Start Game button is pressed
     * @param event event
     */
    private void startGame(ActionEvent event) {
        gameWindow.startChallenge();
    }

    /**
     * Handle when the instructions button is pressed
     * @param event event
     */
    private void showInstructions(ActionEvent event) { gameWindow.loadScene(new InstructionsScene(gameWindow));}

    public void escapePressed() {
        return;
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()== KeyCode.ESCAPE) {
            escapePressed();
        }
    }
}
