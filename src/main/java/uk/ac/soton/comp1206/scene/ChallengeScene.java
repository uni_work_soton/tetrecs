package uk.ac.soton.comp1206.scene;

import javafx.geometry.Pos;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.soton.comp1206.component.GameBlock;
import uk.ac.soton.comp1206.component.GameBoard;
import uk.ac.soton.comp1206.component.Multimedia;
import uk.ac.soton.comp1206.component.PieceBoard;
import uk.ac.soton.comp1206.game.Game;
import uk.ac.soton.comp1206.ui.GamePane;
import uk.ac.soton.comp1206.ui.GameWindow;

/**
 * The Single Player challenge scene. Holds the UI for the single player challenge mode in the game.
 */
public class ChallengeScene extends BaseScene{

    private static final Logger logger = LogManager.getLogger(MenuScene.class);
    protected Game game;

    protected Text scoreText = new Text();
    protected Text levelText = new Text();
    protected Text livesText = new Text();
    protected Text multiplierText = new Text();
    protected Text aimXText = new Text();
    protected Text aimYText = new Text();
    protected Text timeLeftText = new Text();
    protected Text scoreLabel = new Text("Score: ");
    protected Text levelLabel = new Text("Level: ");
    protected Text livesLabel = new Text("Lives: ");
    protected Text multiplierLabel = new Text("Multiplier: ");
    protected Text aimXLabel = new Text("X: ");
    protected Text aimYLabel = new Text("Y: ");
    protected Text timeLeftLabel = new Text("Dont lose a life!: ");
    protected static PieceBoard currentPieceBoard;
    protected static PieceBoard nextPieceBoard;
    protected static GameBoard board;
    protected static Multimedia media = new Multimedia();

    public static PieceBoard getCurrentPieceBoard() {
        return currentPieceBoard;
    }

    public static PieceBoard getNextPieceBoard() {
        return nextPieceBoard;
    }

    /**
     * Create a new Single Player challenge scene
     * @param gameWindow the Game Window
     */
    public ChallengeScene(GameWindow gameWindow) {
        super(gameWindow);
        logger.info("Creating Challenge Scene");
        currentPieceBoard = new PieceBoard(3,3,gameWindow.getWidth()/8,gameWindow.getHeight()/8);
        nextPieceBoard = new PieceBoard(3,3,gameWindow.getWidth()/10,gameWindow.getHeight()/10);
        gameWindow.addEscapeListener(this);
    }

    /**
     * Build the Challenge window
     */
    @Override
    public void build() {
        logger.info("Building " + this.getClass().getName());

        setupGame();

        root = new GamePane(gameWindow.getWidth(),gameWindow.getHeight());

        var challengePane = new StackPane();
        challengePane.setMaxWidth(gameWindow.getWidth());
        challengePane.setMaxHeight(gameWindow.getHeight());
        challengePane.getStyleClass().add("menu-background");
        root.getChildren().add(challengePane);

        var mainPane = new BorderPane();
        challengePane.getChildren().add(mainPane);

        this.board = new GameBoard(game.getGrid(),gameWindow.getWidth()/2,gameWindow.getWidth()/2);
        mainPane.setCenter(board);

        //Handle block on gameBoard grid being clicked
        board.setOnBlockClick(this::blockClicked);

        board.setOnMouseClicked((e)->{
            if(e.getButton()== MouseButton.SECONDARY){
                this.setOnRightClicked(1);
            }
        });

        currentPieceBoard.setOnMouseClicked((e)->{
            this.setOnRightClicked(1);
        });


        //Adding bindings to details about game
        scoreText.textProperty().bind(game.scoreProperty().asString());
        levelText.textProperty().bind(game.levelProperty().asString());
        livesText.textProperty().bind(game.livesProperty().asString());
        multiplierText.textProperty().bind(game.multiplierProperty().asString());
        aimXText.textProperty().bind(game.aimXProperty().asString());
        aimYText.textProperty().bind(game.aimYProperty().asString());
        timeLeftText.textProperty().bind(game.timeLeftProperty().asString());

        //I should really learn CSS
        scoreLabel.setFill(Color.RED);
        scoreText.setFill(Color.RED);
        levelLabel.setFill(Color.RED);
        levelText.setFill(Color.RED);
        livesLabel.setFill(Color.RED);
        livesText.setFill(Color.RED);
        multiplierLabel.setFill(Color.RED);
        multiplierText.setFill(Color.RED);
        aimXLabel.setFill(Color.RED);
        aimXText.setFill(Color.RED);
        aimYLabel.setFill(Color.RED);
        aimYText.setFill(Color.RED);

        var gameDetails = new HBox(scoreLabel,scoreText,levelLabel,levelText,livesLabel,livesText,multiplierLabel,multiplierText);
        gameDetails.setSpacing(gameWindow.getWidth()/10);
        gameDetails.setAlignment(Pos.BOTTOM_CENTER);

        var aimDetails = new HBox(aimXLabel,aimXText,aimYLabel,aimYText);
        mainPane.setBottom(aimDetails);

        mainPane.setTop(gameDetails);

        var nextPieces = new VBox(currentPieceBoard,nextPieceBoard);
        nextPieces.setSpacing(gameWindow.getHeight()/20);
        nextPieces.setAlignment(Pos.CENTER_LEFT);
        mainPane.setRight(nextPieces);
    }

    /**
     * Handle when a block is clicked
     * @param gameBlock the Game Block that was clocked
     */
    private void blockClicked(GameBlock gameBlock) {
        game.blockClicked(gameBlock);
    }

    /**
     * Setup the game object and model
     */
    public void setupGame() {
        logger.info("Starting a new challenge");

        //Start new game
        game = new Game(5, 5);
    }

    /**
     * Initialise the scene and start the game
     */
    @Override
    public void initialise() {
        logger.info("Initialising Challenge");
        game.start();
    }

@Override
    public void keyPressed(KeyEvent keyEvent){
        logger.info("key pressed:");
        var keyCode = keyEvent.getCode();
        if(keyCode==KeyCode.ESCAPE){
            this.escapePressed();
        }else if(keyCode==KeyCode.SPACE || keyCode==KeyCode.R){
            this.swapPieces();
        }else if(keyCode==KeyCode.E || keyCode==KeyCode.C || keyCode==KeyCode.CLOSE_BRACKET){
            //three lefts = right
            this.setOnRightClicked(3);
        }else if(keyCode==KeyCode.Q || keyCode==KeyCode.Z || keyCode==KeyCode.OPEN_BRACKET){
            this.setOnRightClicked(1);
        }else if(keyCode==KeyCode.ENTER || keyCode==KeyCode.X){
            this.blockClicked(this.board.getBlock(game.getAimX(),game.getAimY()));
        }else if(keyCode==KeyCode.W || keyCode==KeyCode.A || keyCode==KeyCode.S || keyCode==KeyCode.D || keyCode==KeyCode.LEFT || keyCode==KeyCode.RIGHT || keyCode==KeyCode.UP || keyCode==KeyCode.DOWN){
            game.adjustAim(keyCode);
        }
    }

    public void escapePressed() {
        logger.info("escape pressed:");
        gameWindow.loadScene(gameWindow.getMenuScene());
    }

    public void setOnRightClicked(int rotationAmount) {
        logger.info("rotating:");
        media.playAudio("sounds/rotate.wav");
        for(int i = 0;i < rotationAmount;i++){
            game.rotatePiece(game.getCurrentPiece(),currentPieceBoard);
        }
    }

    public void swapPieces(){
        logger.info("Swapping pieces:");
        game.swapPieces(currentPieceBoard,nextPieceBoard);
    }
}
