package uk.ac.soton.comp1206.game;

import javafx.application.Platform;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.input.KeyCode;
import javafx.scene.media.MediaPlayer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.soton.comp1206.component.GameBlock;
import uk.ac.soton.comp1206.component.GameBoard;
import uk.ac.soton.comp1206.component.Multimedia;
import uk.ac.soton.comp1206.component.PieceBoard;
import uk.ac.soton.comp1206.scene.BaseScene;
import uk.ac.soton.comp1206.scene.ChallengeScene;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The Game class handles the main logic, state and properties of the TetrECS game. Methods to manipulate the game state
 * and to handle actions made by the player should take place inside this class.
 */
public class Game {

    private static final Logger logger = LogManager.getLogger(Game.class);

    /**
     * Timer that counts down the playing time until a life is lost
     */
    protected static Timer timer;

    /**
     * A bool that tells whether the game is over or not
     */
    protected static Boolean gameOn = true;

    /**
     * Number of rows
     */
    protected final int rows;

    /**
     * Number of columns
     */
    protected final int cols;

    /**
     * The grid model linked to the game
     */
    protected final Grid grid;

    /**
     * The current piece that's to be placed
     */
    protected GamePiece currentPiece;

    /**
     * The next piece that's to be placed
     */
    protected GamePiece nextPiece;

    /**
     * The players score achieved in the game
     */
    protected SimpleIntegerProperty score = new SimpleIntegerProperty(0);

    /**
     * The players current level
     */
    protected SimpleIntegerProperty level = new SimpleIntegerProperty(0);

    /**
     * The amount of lives the player has
     */
    protected SimpleIntegerProperty lives = new SimpleIntegerProperty(3);

    /**
     * The score multiplier
     */
    protected SimpleFloatProperty multiplier = new SimpleFloatProperty(1);

    /**
     * What column is currently targeted (for kb users)
     */
    protected SimpleIntegerProperty aimX = new SimpleIntegerProperty(0);

    /**
     * What row is currently targeted (for kb users)
     */
    protected SimpleIntegerProperty aimY = new SimpleIntegerProperty(0);

    /**
     * Amount of time left before user loses a life
     */
    protected SimpleFloatProperty timeLeft = new SimpleFloatProperty(0);

    /**
     * The media instance for this class, for sounds.
     */
    protected Multimedia media = new Multimedia();

    /**
     * getters and setters and accessors for the above Properties
     *
     */

    public int getAimX() {
        return aimX.get();
    }

    public SimpleIntegerProperty aimXProperty() {
        return aimX;
    }

    public void setAimX(int aimX) {
        this.aimX.set(aimX);
    }

    public int getAimY() {
        return aimY.get();
    }

    public SimpleIntegerProperty aimYProperty() {
        return aimY;
    }

    public void setAimY(int aimY) {
        this.aimY.set(aimY);
    }

    public int getScore() {
        return score.get();
    }

    public SimpleIntegerProperty scoreProperty() {
        return score;
    }

    public void setScore(int score) {
        this.score.set(score);
    }

    public int getLevel() {
        return level.get();
    }

    public SimpleIntegerProperty levelProperty() {
        return level;
    }

    public void setLevel(int level) {
        this.level.set(level);
    }

    public int getLives() {
        return lives.get();
    }

    public SimpleIntegerProperty livesProperty() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives.set(lives);
    }

    public float getMultiplier() {
        return multiplier.get();
    }

    public SimpleFloatProperty multiplierProperty() {
        return multiplier;
    }

    public void setMultiplier(float multiplier) {
        this.multiplier.set(multiplier);
    }

    public float getTimeLeft() { return timeLeft.get(); }

    public SimpleFloatProperty timeLeftProperty() { return timeLeft; }

    public void setTimeLeft(float timeLeft) { this.timeLeft.set(timeLeft); }

    /**
     * Create a new game with the specified rows and columns. Creates a corresponding grid model.
     * @param cols number of columns
     * @param rows number of rows
     */
    public Game(int cols, int rows) {
        this.cols = cols;
        this.rows = rows;

        //Create a new grid model to represent the game state
        this.grid = new Grid(cols,rows);
    }

    /**
     * Start the game
     */
    public void start() {
        logger.info("Starting game");
        initialiseGame();
    }

    /**
     * Initialise a new game and set up anything that needs to be done at the start
     */
    public void initialiseGame() {
        logger.info("Initialising game");

        //Initialise the first piece
        currentPiece = this.spawnPiece();
        nextPiece = this.spawnPiece();

        //Set pieces on the challengeScene
        ChallengeScene.getCurrentPieceBoard().setPiece(currentPiece);
        ChallengeScene.getNextPieceBoard().setPiece(nextPiece);

        //Setup timer
        this.timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() { Platform.runLater(()->gameLoop());}
        };
        timer.schedule(timerTask, getTimerDelay());
    }

    /**
     * Handle what should happen when a particular block is clicked
     * @param gameBlock the block that was clicked
     */
    public void blockClicked(GameBlock gameBlock) {
        //Get the position of this block
        int x = gameBlock.getX();
        int y = gameBlock.getY();

        if(grid.canPlayPiece(this.currentPiece,x,y)){
            logger.info("Attempting to place piece {}",this.currentPiece.toString());
            grid.playPiece(this.currentPiece,x,y);
            this.afterPiece();
        }else{
            logger.info("Failed to place piece at x:{},y:{}",x,y);
        }

        aimX.set(x);
        aimY.set(y);
    }

    /**
     * Handles what should happen after a block has been placed, eg testing whether a full line exists
     */
    public void afterPiece(){
        logger.info("Moving to next piece");
        this.nextPiece();
        ChallengeScene.getCurrentPieceBoard().clearBoard();
        ChallengeScene.getNextPieceBoard().clearBoard();
        ChallengeScene.getCurrentPieceBoard().setPiece(currentPiece);
        ChallengeScene.getNextPieceBoard().setPiece(nextPiece);
        //Checks if theres a row/column thats full
        logger.info("Checking whether a full row/column exists");
        ArrayList<Integer> rowsFull = new ArrayList<Integer>();
        ArrayList<Integer> colsFull = new ArrayList<Integer>();

        //Clears rows/cols if full
        //Fills up columns full and takes them away as 0's are found
        for(int col = 0;col<cols;col++){
            colsFull.add(col);
        }

        //Fills up rowsFull/colsFull if found full row/col
        for(int row = 0; row<this.rows;row++){
            boolean rowFull = true;
            for(int col = 0; col<this.cols ; col++){
                if(this.getGrid().get(col,row)==0){
                    rowFull = false;
                    //Integer.valueOf is used to access ArrayList.remove(Object) implementation, which removes by value instead of by index
                    colsFull.remove(Integer.valueOf(col));
                }
            }
            if(rowFull){
                rowsFull.add(row);
            }
        }

        //Clears row,col if in rowsFull/colsFull
        for(int row = 0; row<this.rows;row++){
            for(int col = 0; col<this.cols;col++){
                if(rowsFull.contains(row)){
                    logger.info("Full row found! Row {}",row);
                    this.getGrid().set(col,row,0);
                }
                if(colsFull.contains(col)){
                    logger.info("Full col found! Col {}",col);
                    this.getGrid().set(col,row,0);
                }
            }
        }

        //Play appropriate sound based on how many rows/cols cleared
        logger.info("Attempting post place audio:");
        if(rowsFull.size()+colsFull.size()==0){
            media.playAudio("sounds/place.wav");
        }else if(rowsFull.size()+colsFull.size()==1){
            media.playAudio("sounds/clear.wav");
        }else if(rowsFull.size()+colsFull.size()>1){
            media.playAudio("sounds/explode.wav");
        }

        //Adding score - rowsFull.size()*colsFull.size() calculates the number of intersecting blocks that are are in multiple cleared lines
        this.score(rowsFull.size()+colsFull.size(),(rowsFull.size()*rows + colsFull.size()*cols)-(rowsFull.size()*colsFull.size()));

        //Multiplier calculator
        if(rowsFull.size()+colsFull.size()>0){
            setMultiplier(getMultiplier()+1);
        }else{
            setMultiplier(1);
        }

        //Level calculator
        setLevel(getScore()/1000);

        //Reset timer to full
        //Cancels the timer and clears the tasks, and then replaces the empty timer by a new one. Because doing just one wasnt enough apparently
        timer.cancel();
        timer.purge();
        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() { Platform.runLater(()->gameLoop());}
        }, getTimerDelay());

        if(!gameOn){
            //do game over stuff
        }
    }

    /**
     * Get the grid model inside this game representing the game state of the board
     * @return game grid model
     */
    public Grid getGrid() {
        return grid;
    }

    /**
     * Get the number of columns in this game
     * @return number of columns
     */
    public int getCols() {
        return cols;
    }

    /**
     * Get the number of rows in this game
     * @return number of rows
     */
    public int getRows() {
        return rows;
    }

    /**
     * Spawns a new piece that can be placed
     * @return the new gamepiece
     */
    public GamePiece spawnPiece() {
        Random random = new Random();
        return(GamePiece.createPiece(random.nextInt(15)));
    }

    /**
     * Uses spawnPiece() to cycle to the next piece
     */
    public void nextPiece() {
        this.currentPiece = this.nextPiece;
        this.nextPiece = this.spawnPiece();
        ChallengeScene.getCurrentPieceBoard().clearBoard();
        ChallengeScene.getNextPieceBoard().clearBoard();
        ChallengeScene.getCurrentPieceBoard().setPiece(currentPiece);
        ChallengeScene.getNextPieceBoard().setPiece(nextPiece);
    }

    public void rotatePiece(GamePiece piece, PieceBoard board) {
        piece.rotate();
        board.clearBoard();
        board.setPiece(this.currentPiece);
    }

    public void swapPieces(PieceBoard currentBoard, PieceBoard nextBoard){
        var temp = GamePiece.createPiece(currentPiece.getValue()-1);
        var temp2 = GamePiece.createPiece(nextPiece.getValue()-1);
        this.currentPiece =temp2;
        this.nextPiece = temp;

        currentBoard.clearBoard();
        currentBoard.setPiece(this.currentPiece);
        nextBoard.clearBoard();
        nextBoard.setPiece(this.nextPiece);

    }

    /**
     * Adds score after a line clear. It should add a score based on the following formula:
     * number of lines*number of grid blocks cleared*10*the current multiplier
     * @param lines the number of lines cleared
     * @param blocks the amount of blocks that were cleared
     */
    public void score(int lines, int blocks){
        setScore(getScore()+(int)(lines*blocks*10*getMultiplier()));
    }

    public GamePiece getCurrentPiece() {
        return currentPiece;
    }

    public GamePiece getNextPiece() {
        return nextPiece;
    }

    public void adjustAim(KeyCode keyCode) {
        if ((keyCode == KeyCode.S || keyCode == KeyCode.DOWN) && aimY.get() < rows - 1) {
            aimY.set(aimY.get() + 1);
        } else if ((keyCode == KeyCode.W || keyCode == KeyCode.UP) && aimY.get() > 0) {
            aimY.set(aimY.get() - 1);
        } else if ((keyCode == KeyCode.D || keyCode == KeyCode.RIGHT) && aimX.get() < cols - 1) {
            aimX.set(aimX.get() + 1);
        } else if ((keyCode == KeyCode.A || keyCode == KeyCode.LEFT) && aimX.get() > 0) {
            aimX.set(aimX.get() - 1);
        }
    }

    /**
     * Used to find the delay for the timer. changes based on level
     * @return returns the delay in millis
     */
    public int getTimerDelay(){
        int timerDelay = 12000-(500*level.get());
        if(timerDelay<2500){
            return 2500;
        }else{
            return timerDelay;
        }
    }

    public void gameLoop() {
        if (this.lives.get() == 0) {
            logger.info("game over!");
            media.playAudio("sounds/fail.wav");
            gameOn=false;

        } else {
            logger.info("User failed to place in time:");
            this.lives.set(this.lives.get() - 1);
            this.nextPiece();
            this.multiplier.set(1);
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(() -> gameLoop());
                }
            }, getTimerDelay());
        }
    }
}
