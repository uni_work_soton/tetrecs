package uk.ac.soton.comp1206.event;

import javafx.scene.input.KeyEvent;

public interface EscapePressedListener {
    public void keyPressed(KeyEvent keyEvent);
}
