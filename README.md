# tetrecs

TetrECS is a first year project I worked on at university. It's a game similar to tetris, but you have to fill out an entire row OR column in a grid with custom shapes. 

It includes multiple stages, multiplayer leaderboards via websockets to a student-wide server, and design via javaFX.

![In game controls page](/900px-Controls.png "Tutorial/Controls page")
